import os
import sys
import threading
from time import sleep
from concurrent import futures
from typing import Any
import grpc
import twr_pb2
import twr_pb2_grpc
import time


class Direction:
    LEFT = 0
    RIGHT = 1


topology: list[int]
messages: int = 0


class Node(threading.Thread, twr_pb2_grpc.TwrServicer):
    my_order: int
    node_id: int
    addr_l: str  # left skeleton
    addr_r: str  # right skeleton
    neight_addr_l: str
    neight_addr_r: str
    server: Any

    leaderId: int = -1
    defeated: bool = False
    phase: int = 0
    received_election: int = -1
    received_yes: set[int]

    def getNodeByDistance(self, distance: int, direction: Direction) -> int:
        node: int

        if direction == Direction.LEFT:
            node = self.my_order + distance
        else:
            node = self.my_order - distance

        if node <= 0:
            node += len(topology)
        elif node > len(topology):
            node -= len(topology)

        return topology[node - 1]

    def log(self, request, text: str = ""):
        t = time.localtime()
        current_time = time.strftime("%H:%M:%S", t)
        print(
            f"<{self.node_id}, {current_time}, {request.senderid}, {request.receiverid}>: {request.content} {text}")

    def __init__(self, my_order: int, node_id: int, addr_l: str,
                 addr_r: str, neight_addr_l: str,
                 neight_addr_r: str):
        threading.Thread.__init__(self)
        self.thread_name = "NONE"
        self.thread_ID = node_id
        self.my_order = my_order
        self.node_id = node_id
        self.addr_l = addr_l
        self.addr_r = addr_r
        self.neight_addr_l = neight_addr_l
        self.neight_addr_r = neight_addr_r
        self.received_yes = set()

    def findLeader(self):
        if self.defeated:
            return

        self.received_election = 0
        self.phase += 1
        to_left: int = self.getNodeByDistance(self.phase, Direction.LEFT)
        to_right: int = self.getNodeByDistance(self.phase, Direction.RIGHT)

        # send message to left neighbor
        self.sendEM(to_left, Direction.LEFT)
        # send message to right neighbor
        self.sendEM(to_right, Direction.RIGHT)

    def send(self, receiverid: int, senderid: int, content: str, direction: int):
        global messages
        messages += 1

        channel: str
        if direction == Direction.LEFT:
            channel = self.neight_addr_l
        else:
            channel = self.neight_addr_r

        with grpc.insecure_channel(channel) as channel:
            request = twr_pb2.twrRequest(
                receiverid=receiverid, senderid=senderid, content=content, direction=direction)
            self.log(request, "sent")
            stub = twr_pb2_grpc.TwrStub(channel)
            response = stub.sendMessage(request)  # unused

    def standDefeated(self):
        if self.defeated == False:
            self.defeated = True

    def sendTo(self, receiverId: int, content: str, direction: int):
        self.send(receiverId, self.node_id, content, direction)

    def replyMessage(self, request, content: str):
        # Send to left -> i received from right -> reply to right
        replyDir: Direction = Direction.LEFT if request.direction == Direction.RIGHT else Direction.RIGHT
        self.sendTo(request.senderid, content, replyDir)

    def sendEM(self, receiverid: int, direction: Direction):
        if self.defeated:
            return
        self.sendTo(receiverid, "EM", direction)

    def forwardMessage(self, request: twr_pb2.twrRequest):
        self.send(request.receiverid, request.senderid,
                  request.content, request.direction)

    def processYes(self, request: twr_pb2.twrRequest):
        if self.defeated:
            return

        self.received_election += 1
        is_overlapping: bool = request.senderid in self.received_yes
        self.received_yes.add(request.senderid)

        if is_overlapping:
            # There are overlaps, i'm a leader
            self.leaderId = self.node_id
            self.sendTo(self.node_id, "LEADER", Direction.LEFT)
            return

        if self.received_election == 2:
            self.findLeader()

    def processMessage(self, request: twr_pb2.twrRequest):
        if request.content == "EM":
            if request.senderid < self.node_id:
                self.replyMessage(request, "YES")

        if request.content == "YES":
            self.processYes(request)

    def sendMessage(self, request, context):
        """ RECEIVE MESSAGE """
        if request.senderid < self.node_id:
            self.standDefeated()

        self.log(request, "received")
        if self.node_id == request.receiverid:
            # For me
            if self.leaderId == -1:
                self.processMessage(request)
        else:
            if request.content == "LEADER":
                self.leaderId = request.senderid
            self.forwardMessage(request)

        context.set_code(grpc.StatusCode.OK)
        return twr_pb2.twrResponse()

    def run(self):
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=100))
        twr_pb2_grpc.add_TwrServicer_to_server(self, self.server)
        self.server.add_insecure_port(self.addr_l)
        self.server.add_insecure_port(self.addr_r)
        self.server.start()
        self.server.wait_for_termination()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            f"Invalid number of arguments, should be 1, argc = {len(sys.argv)}")
        exit(1)

    error: bool = False
    topology = [int(x) for x in sys.argv[1].split(",")]
    nodes: int = len(topology)
    ring: list[Node] = []

    for order in range(1, nodes + 1):
        ports_l = 60000
        ports_r = 61000
        nodeR: int = nodes if order - 1 <= 0 else order - 1
        nodeL: int = 1 if order + 1 > nodes else order + 1
        addr_l: str = "localhost:" + str(order + ports_l)
        addr_r: str = "localhost:" + str(order + ports_r)
        neigh_addr_l: str = "localhost:" + str(nodeL + ports_r)
        neight_add_r: str = "localhost:" + str(nodeR + ports_l)
        ring.append(Node(order, topology[order - 1], addr_l, addr_r,
                         neigh_addr_l, neight_add_r))

    for node in ring:
        node.start()

    leader: int = min(topology)

    sleep(0.2)  # Wait for all servers to start
    for node in ring:
        threading.Thread(target=node.findLeader).start()

    for node in ring:
        while node.leaderId == -1:
            sleep(0.1)
        if (node.leaderId != leader):
            error = True
            print(
                f"Error, node {node.node_id} thinks that leader is {node.leaderId}, but leader is {leader}", file=sys.stderr)

    print(f"{messages};{len(topology)}", file=sys.stderr)

    if (error):
        os._exit(1)

    os._exit(0)
