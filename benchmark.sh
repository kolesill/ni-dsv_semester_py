#!/bin/bash

echo -n "" > logs.txt
echo "<number of messages>;<number of nodes>" > results.txt

for line in $( cat data.txt ); do
    echo "INSTANCE: $line ==================" >> logs.txt
    python3 twr.py $line 1>>logs.txt 2>>results.txt
done